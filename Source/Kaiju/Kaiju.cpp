// Copyright Epic Games, Inc. All Rights Reserved.

#include "Kaiju.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Kaiju, "Kaiju" );
