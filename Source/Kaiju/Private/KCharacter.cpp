// Fill out your copyright notice in the Description page of Project Settings.


#include "KCharacter.h"
#include "..\Public\KCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/KHealthComponent.h"
#include "KWeapon.h"



AKCharacter::AKCharacter()
{
 	
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->bUsePawnControlRotation = true;
	SpringArmComp->SetupAttachment(RootComponent);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp);

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_EngineTraceChannel1, ECR_Ignore);
	
	HealthComp = CreateDefaultSubobject<UKHealthComponent>(TEXT("HealthComp"));
	
	WeaponAttachSocketName = "WeaponSocket";

	bDied = false;

	/*SprintSpeedMultiplier = 2.0f;
	bIsRunning = false;
	DoubleJumpHeight = 500.f;*/
}

void AKCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	CurrentWeapon = GetWorld()->SpawnActor<AKWeapon>(StarterWeaponClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
	if (CurrentWeapon)
	{
		CurrentWeapon->SetOwner(this);
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, "WeaponSocket");
	}

	HealthComp->OnHealthChanged.AddDynamic(this, &AKCharacter::OnHealthChanged);
}


void AKCharacter::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
}

void AKCharacter::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}

void AKCharacter::OnHealthChanged(UKHealthComponent* OwningHealthComp, float Health, float HealthDelta, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{

	if (Health <= 0.0f && !bDied)
	{
		bDied = true;

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		
		DetachFromControllerPendingDestroy();

		SetLifeSpan(10.0f);
		CurrentWeapon->SetLifeSpan(10.0f);
	}

}

void AKCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AKCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AKCharacter::MoveRight);

	PlayerInputComponent->BindAxis("LookUp", this, &AKCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &AKCharacter::AddControllerYawInput);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AKCharacter::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AKCharacter::StopFire);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AKCharacter::DoubleJump);
	//PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);

	/*PlayerInputComponent->BindAction("Running", IE_Pressed, this, &AKCharacter::StartRunning);
	PlayerInputComponent->BindAction("Running", IE_Released, this, &AKCharacter::StopRunning);*/

}

void AKCharacter::MoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value);
}

void AKCharacter::MoveRight(float Value)
{
	AddMovementInput(GetActorRightVector() * Value);
}

//Eyes camera/pawn 
FVector AKCharacter::GetPawnViewLocation() const
{
	if (CameraComp)
	{
		return CameraComp->GetComponentLocation();
	}

	return Super::GetPawnViewLocation();
}

void AKCharacter::DoubleJump()
{
	Super::Jump();
	JumpMaxCount = 2;
	/*DoubleJumpCounter++;
	if (DoubleJumpCounter == 2)
	{
		bWasJumping = true;
		LaunchCharacter(FVector(0.0f, 0.0f, DoubleJumpHeight), false, true);
	}*/

}


//void AKCharacter::StartRunning()
//{
//	bIsRunning = true;
//	GetCharacterMovement()->MaxWalkSpeed *= SprintSpeedMultiplier;
//}
//
//void AKCharacter::StopRunning()
//{
//	bIsRunning = false;
//	GetCharacterMovement()->MaxWalkSpeed /= SprintSpeedMultiplier;
//}
////void AKCharacter::Landed(const FHitResult& Hit)
////{
////	DoubleJumpCounter = 0;
////}