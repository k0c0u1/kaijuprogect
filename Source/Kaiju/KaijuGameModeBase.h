// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KaijuGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class KAIJU_API AKaijuGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
